const player1 = "X"
const player2 = "O"
const WINNING_COMBINATIONS = [
    [(1), (2), (3)],
    [(4), (5), (6)],
    [(7), (8), (9)],
    [(1), (4), (7)],
    [(2), (5), (8)],
    [(3), (6), (9)],
    [(1), (5), (9)],
    [(3), (5), (7)]
];
const array_X = [

];
const array_O = [

];

function handleClick(id) {
    const cell = document.getElementById(id);
    if (array_X.includes(id) || array_O.includes(id)) {
        alert("cell is occupied, try a different one!")
    } else {

        addCrossIcon(cell);
        cell.setAttribute('disabled', true);
        array_X.push(id)

        if (checkWin(array_X, player1)) {
            return;
        } 
        if (checkDraw(array_X, array_O,player1,player2)){
            return;
        } 

        botPlayer(array_O, array_X);
        checkWin(array_O, player2);    
    }   
}

//function to check for a win 
function checkWin(array_para, currentPlayer) {
    const hasArrayWon =
        WINNING_COMBINATIONS.some(winning_combination => {
            return winning_combination.every(number => (
                (array_para.includes(number))
            ))
        })
    if (hasArrayWon) {
        alert("Player " + currentPlayer + " wins!");
        return true; 
    } return false; 
}

//function to end the Game 
function endGame(){
    
}

//function to check for a draw
function checkDraw(array_X, array_O, player1,player2) {
    var arrLength = 0;
    var arrLength = ((array_X.length) + (array_O.length));
    const player1NotWon = checkWin(array_X, player1) == false;
    const player2NotWon = checkWin(array_O, player2) == false;
    const gameComplete = arrLength == 9;
    if (gameComplete && player1NotWon && player2NotWon) {
        alert("It's a Draw! Game over!");
    } return;
}

//function simulating a botplayer
function botPlayer(array_O, array_X) {
    //function to create an array indicating free spaces
    const initialArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const arrTaken = array_O.concat(array_X);
    const arrFree = initialArray.filter(item => !arrTaken.includes(item));

    // choose random free space(from arrFree) and place "O"
    let randomSlot = Math.floor(Math.random() * arrFree.length);
    let randomFreeCell = arrFree[randomSlot];
    let cell = document.getElementById(randomFreeCell);
    
    addCircleIcon(cell);
    array_O.push(randomFreeCell);
    

}

//function to add a cross svg icon 
function addCrossIcon(node) {
    const iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const iconPath = document.createElementNS('http://www.w3.org/2000/svg','path');
  
  
    iconSvg.setAttribute('fill', '#000000');
    iconSvg.setAttribute('viewBox', '-150 -150 800 800');
    iconSvg.setAttribute('stroke', 'black');
    iconSvg.classList.add('cross-icon');
  
    iconPath.setAttribute(
      'd',
      "M285.08,230.397L456.218,59.27c6.076-6.077,6.076-15.911,0-21.986L423.511,4.565c-2.913-2.911-6.866-4.55-10.992-4.55c-4.127,0-8.08,1.639-10.993,4.55l-171.138,171.14L59.25,4.565c-2.913-2.911-6.866-4.55-10.993-4.55c-4.126,0-8.08,1.639-10.992,4.55L4.558,37.284c-6.077,6.075-6.077,15.909,0,21.986l171.138,171.128L4.575,401.505c-6.074,6.077-6.074,15.911,0,21.986l32.709,32.719c2.911,2.911,6.865,4.55,10.992,4.55c4.127,0,8.08-1.639,10.994-4.55l171.117-171.12l171.118,171.12c2.913,2.911,6.866,4.55,10.993,4.55c4.128,0,8.081-1.639,10.992-4.55l32.709-32.719c6.074-6.075,6.074-15.909,0-21.986L285.08,230.397z");
    iconPath.setAttribute('stroke-linecap', 'round');
    iconPath.setAttribute('stroke-linejoin', 'round');
    iconPath.setAttribute('stroke-width', '2');
  
    iconSvg.appendChild(iconPath);
  
    return node.appendChild(iconSvg);
  }
//function to add a circle svg icon 
  function addCircleIcon(node) {
    const iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const iconPath = document.createElementNS('http://www.w3.org/2000/svg','path');
  
  
    iconSvg.setAttribute('fill', '#000000');
    iconSvg.setAttribute('viewBox', '-150 -150 800 800');
    iconSvg.setAttribute('stroke', 'black');
    iconSvg.classList.add('circle-icon');
  
    iconPath.setAttribute(
      'd', "M468.065,234.032c0,129.256-104.776,234.032-234.032,234.032C104.776,468.065,0,363.288,0,234.032C0,104.776,104.776,0,234.033,0C363.289,0,468.065,104.776,468.065,234.032z");
    iconPath.setAttribute('stroke-linejoin', 'round');
    iconPath.setAttribute('stroke-width', '2');
  
    iconSvg.appendChild(iconPath);
  
    return node.appendChild(iconSvg);
  }